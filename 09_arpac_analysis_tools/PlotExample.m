time_act_outputs = sysvector.actuator_outputs_0.output_0.time;

%data from motors
act_output0 = sysvector.actuator_outputs_0.output_0.data;
act_output1 = sysvector.actuator_outputs_0.output_1.data;
act_output2 = sysvector.actuator_outputs_0.output_2.data;
act_output3 = sysvector.actuator_outputs_0.output_3.data;

%output of angle rate controllers
time_lin_control = sysvector.lin_control_0.u_rate_0.time;
urate0 = sysvector.lin_control_0.u_rate_0.data;
urate1 = sysvector.lin_control_0.u_rate_1.data;
urate2 = sysvector.lin_control_0.u_rate_2.data;
uvel2 = sysvector.lin_control_0.u_vel_2.data;

%Calculate motor outputs from controller outputs
motor_0 = zeros(size(urate0));
motor_1 = motor_0;
motor_2 = motor_0;
motor_3 = motor_0;

%How mixers works?
%https://discuss.px4.io/t/how-mixer-work/3886/2
for k = 1:size(motor_0,1)
    motor_0(k) =    interp1([0 1],[paramvector.pwm_min.data,paramvector.pwm_max.data], ...
                    -0.707107*urate0(k)+0.707107*urate1(k)+urate2(k)+uvel2(k) );
    motor_1(k) =    interp1([0 1],[paramvector.pwm_min.data,paramvector.pwm_max.data], ...
                    0.707107*urate0(k)-0.707107*urate1(k)+urate2(k)+uvel2(k) );
    motor_2(k) =    interp1([0 1],[paramvector.pwm_min.data,paramvector.pwm_max.data], ...
                    0.707107*urate0(k)+0.707107*urate1(k)-urate2(k)+uvel2(k) );
    motor_3(k) =    interp1([0 1],[paramvector.pwm_min.data,paramvector.pwm_max.data], ...
                    -0.707107*urate0(k)-0.707107*urate1(k)-urate2(k)+uvel2(k) );                
end

%check whether a topic exists in the log file...
%isfield(sysvector,'lin_control_0')
%isfield(sysvector.lin_control_0,'r_rate_3')

%time_r_angle_2 = sysvector.lin_control_0.r_angle_2.time;
%data_r_angle_2 = sysvector.lin_control_0.r_angle_2.data;

%time_y_angle_2 = sysvector.lin_control_0.y_angle_2.time;
%data_y_angle_2 = sysvector.lin_control_0.y_angle_2.data;

%plot(time_r_angle_2,data_r_angle_2)
%hold on
%plot(time_y_angle_2,data_y_angle_2)

plot(time_lin_control, motor_0)
hold on
plot(time_act_outputs, act_output0,'--')

plot(time_lin_control, motor_1)
hold on
plot(time_act_outputs, act_output1,'--')

plot(time_lin_control, motor_2)
hold on
plot(time_act_outputs, act_output2,'--')

plot(time_lin_control, motor_3)
hold on
plot(time_act_outputs, act_output3,'--')