rrate2 = sysvector.lin_control_0.r_rate_2.data;
yrate2 = sysvector.lin_signal_0.y_rate_2.data;
raw_yrate2 = sysvector.lin_signal_0.raw_y_rate_2.data;

time_signal = sysvector.lin_signal_0.raw_y_rate_2.time;
time_control = sysvector.lin_control_0.r_rate_2.time;

figure(1)
plot(time_signal,yrate2,'b')
hold on
plot(time_signal,raw_yrate2,'r')


figure(2)
plot(time_signal,yrate2,'b')
hold on
plot(time_control,rrate2,'r')