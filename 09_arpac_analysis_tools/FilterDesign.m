%gyro filter

%data from motors
act_output0 = sysvector.actuator_outputs_0.output_0.data;
act_output1 = sysvector.actuator_outputs_0.output_1.data;
act_output2 = sysvector.actuator_outputs_0.output_2.data;
act_output3 = sysvector.actuator_outputs_0.output_3.data;
time_act_outputs = sysvector.actuator_outputs_0.output_0.time;

%data from lin_control and lin_signal
yrate2 = sysvector.lin_control_0.y_rate_2.data;
rrate2 = sysvector.lin_control_0.r_rate_2.data;
erate2 = rrate2 - yrate2;
urate2 = sysvector.lin_control_0.u_rate_2.data;
time_control = sysvector.lin_control_0.y_rate_2.time;

%original signal
Fs = 200 %Hz
Y = fft(yrate2);
L = length(yrate2);
P2 = abs(Y/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

figure(1);
plot(f,P1,'DisplayName','original');
title('Single-Sided Amplitude Spectrum of y_{rate}_2(t)');
xlabel('f (Hz)');
ylabel('|Y(f)|');

figure(2);
plot(time_control,1000*yrate2,'.','DisplayName','original');

%filtered signal
for fc = [30 25 20 15]
    fs = 200;

    [num,den] = butter(2,fc/(fs/2));
    %freqz(num,den)
    yrate2_filtered = filter(num,den,yrate2);

    Fs = 200 %Hz
    Y = fft(yrate2_filtered);
    L = length(yrate2_filtered);
    P2 = abs(Y/L);
    P1 = P2(1:(L/2+1));
    P1(2:end-1) = 2*P1(2:end-1);
    f = Fs*(0:(L/2))/L;
    
    figure(1)
    hold on
    plot(f,P1,'.','DisplayName',['fc =' mat2str(fc)]);
    title('Single-Sided Amplitude Spectrum of y_{rate}_2 filtered(t)');
    xlabel('f (Hz)');
    ylabel('|Y(f)|');
    legend show

    figure(2)
    hold on
    title('Filtered y_{rate}_2 signals') 
    plot(time_control,1000*yrate2_filtered,'.','DisplayName',['fc =' mat2str(fc)]);
    legend show
end



