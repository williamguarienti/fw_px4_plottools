%plot(sysvector.vehicle_global_position_0.alt.time,sysvector.vehicle_global_position_0.alt.data) 
hold on

%plot(sysvector.distance_sensor_0.current_distance.time,sysvector.distance_sensor_0.current_distance.data)

radio_strenght = sysvector.radio_status_0.rssi.data;
radio_strenght = radio_strenght/1.9 - 127;

plot(sysvector.radio_status_0.rssi.time, radio_strenght);

line_90 = [-90 -90];
time_90 = [60 140];
plot(time_90,line_90)